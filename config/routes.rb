Rails.application.routes.draw do

  devise_for :users

  authenticated :user do
    root :to => "patients#index", as: :all_patients
  end

  root :to => 'welcome#index'

  resources :patients do
    collection do
      get :autocomplete
    end
    resources :visits do
      resources :prescription
    end
  end
end
