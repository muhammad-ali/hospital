$ ->
  $('#patient_search').typeahead
    name: "patient"
    remote: "/patients/autocomplete?query=%QUERY"