class VisitsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_patient
  before_action :set_visit, only: [:edit, :show, :update, :destroy]
  
  def new
    @visit = @patient.visits.build 
  end

  def create
    visit = @patient.visits.create(visit_params)
    if visit
          flash[:info] = "Visit date has been fixed"
          redirect_to patient_visit_path(@patient, visit)
        else
          render 'new'
    end
  end

  def update
      if @visit.update(visit_params)
        flash[:success] = "visit date has been updated"
        redirect_to patient_visit_path(@patient, @visit)
      else
        render 'edit'
      end
  end

  def edit
  end

  def destroy
    @visit.destroy
    flash[:alert] = "Vist has been canceled"
     
    redirect_to patient_visits_path(@patient)
  end

  def show
  end

  private
    def set_patient
      @patient = Patient.find(params[:patient_id])
    end

    def set_visit
      @visit = @patient.visits.find(params[:id])
    end

  def visit_params
    params.require(:visit).permit(:next)
  end
end
