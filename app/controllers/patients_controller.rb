class PatientsController < ApplicationController

	before_action :authenticate_user!

	def new
		@patient = Patient.new 
	end

	def create
		@patient = Patient.new(patient_params)
		if @patient.save
      		flash[:info] = "Patient has been saved"
      		redirect_to @patient
      	else
      		render 'new'
		end
	end

	def show
		@patient = Patient.find(params[:id])
		@visits = @patient.visits.paginate(:page => params[:page])
	end

	def index
		if params[:query].present?
      		@patients = Patient.search(params[:query], page: params[:page])
    	else
      		@patients = Patient.paginate(page: params[:page])
    	end
	end

	def edit
		@patient = Patient.find(params[:id])
	end

	def update
	    @patient = Patient.find(params[:id])
	    if @patient.update(patient_params)
	      flash[:success] = "Patient updated"
	      redirect_to @patient
	    else
	      render 'edit'
	    end
  	end

  def destroy
	@patient = Patient.find(params[:id])
	if  @patient.destroy
	  flash[:alert] = "Patient deleted"
	end
	 
	redirect_to patients_path
  end

  def autocomplete
    render json: Patient.search(params[:query], autocomplete: true, limit: 10).map(&:name)
  end

	private
	def patient_params
		params.require(:patient).permit(:name, :gender, :DOB)
	end
end
