class Patient < ActiveRecord::Base
	has_many :visits

	searchkick autocomplete: ['name']

	def age
	  now = Time.now.utc.to_date
	  now.year - self.DOB.year - ((now.month > self.DOB.month || (now.month == self.DOB.month && now.day >= self.DOB.day)) ? 0 : 1)
	end
end
