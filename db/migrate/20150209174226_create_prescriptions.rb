class CreatePrescriptions < ActiveRecord::Migration
  def change
    create_table :prescriptions do |t|
      t.string :name
      t.string :quantity
      t.references :visit, index: true

      t.timestamps null: false
    end
    add_foreign_key :prescriptions, :visits
  end
end
