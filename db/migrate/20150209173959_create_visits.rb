class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.date :next
      t.references :patient, index: true

      t.timestamps null: false
    end
    add_foreign_key :visits, :patients
  end
end
